import { CadencePage } from './app.po';

describe('cadence App', function() {
  let page: CadencePage;

  beforeEach(() => {
    page = new CadencePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
