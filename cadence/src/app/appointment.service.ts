import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AppointmentService {

  private appointmentCreatedSource = new Subject<string>();

  appointmentCreated$ = this.appointmentCreatedSource.asObservable();

  constructor() { }

  getList() {
    return JSON.parse(localStorage.getItem('appointments'));
  }

  save(appointment) {
    let appointments = this.getList();
    appointments.push(appointment);
    
    localStorage.setItem('appointments', JSON.stringify(appointments));
    this.appointmentCreatedSource.next(appointment);
  }
}
