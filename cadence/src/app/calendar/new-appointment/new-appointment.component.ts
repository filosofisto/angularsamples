import { Component, OnInit } from '@angular/core';
import { Appointment } from '../../appointment';
import { AppointmentService } from '../../appointment.service';

@Component({
  selector: 'cadence-new-appointment',
  templateUrl: './new-appointment.component.html',
  styleUrls: ['./new-appointment.component.css']
})
export class NewAppointmentComponent implements OnInit {

  model = new Appointment();

  constructor(private appointmentService: AppointmentService) { 
    
  }

  ngOnInit() {
  }

  save() {
    this.appointmentService.save(this.model);
  }

}
