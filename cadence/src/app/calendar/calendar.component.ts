import { Component, OnInit } from '@angular/core';
import { AppointmentService } from '../appointment.service';

@Component({
  selector: 'cadence-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  appointments = [];

  constructor(private appointmentService: AppointmentService) { 
    appointmentService.appointmentCreated$.subscribe(appointment => {
      this.loadAppointments();
    });
  }

  ngOnInit() {
    this.loadAppointments();
  }

  loadAppointments() {
    this.appointments = this.appointmentService.getList();
  }


}
